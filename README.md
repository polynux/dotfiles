# dotfiles

## Installation
1. Run `git clone https://github.com/polynux/dotfiles ~/dotfiles`, `cd ~/dotfiles`, `./install.sh`.
2. After that, close and reopen terminal to view zsh, or run `source ~/.zshrc`.
